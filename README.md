Aquarium VR
===========

This constructs a virtual marine world, in which artificial fishes behave as
they do in realistic physical space. To immerse oneself in the world, Cardboard
(VR) box is required.